import { post } from 'axios'

import apolloClient from '../apollo/apolloClient'
import router from '../router'

/**
 * 
 * Make a search for providers. Then, mutate the apollo cache with the new data
 * 
 * @param {string} name - the name of the route to navigate to
 * @param {object} params - the params to submit
 * 
 */
export const submitSearchForm = (name, params) => {
  router.push({ name, params })

  apolloClient.mutate({
    mutation: require('../graphql/mutations/routeParams.gql'),
    variables: {
      __typename: 'RouteParams',
      category: params.category,
      lastName: params.lastName,
      location: params.location,
      useParameters: true,
      specialties: params.specialties
    }
  })
}

/**
 * 
 * Modify form data before sending it to the server. 
 * This is needed because the secure email server expects some values to have particular properties.
 * 
 * @param data {object} - values from forms
 * 
 * @return output {string} - the modified form data as a URL encoded string
 */
export const modifyRequestApptData = (data) => {
  const output = data

  output['Field1'] = data.field1
  output['Field2'] = data.field2
  output['Field3'] = data.field3

  delete output.field1
  delete output.field2
  delete output.field3

  return new URLSearchParams(output).toString()
}

/**
 * 
 * @param url {string} - the url of the secure email processing app
 * @param data {string} - the form data as a URL encoded string
 * 
 * @return {Promise} - a Promise to resolve after all requests have finished
 * 
 */
export const postSecureData = async (url, data) => {
  return await post(url, data, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  })
}