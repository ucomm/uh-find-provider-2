<?php

namespace UHFP\Admin;

use UHFP\Queries\Queries;

class Options {

  public $query;

  public function __construct()
  {
    $this->query = new Queries();
  }

  public function init() {
    add_action('admin_init', [$this, 'prepareSettingsSections']);
  }
  
  public function prepareSettingsSections() {
    $this->registerSettings();
    $this->addSettingsSection();
  }

  public function registerSettings() {
    register_setting('uhfp-settings', 'uhfp-settings');
  }

  public function addSettingsSection() {

    add_settings_section(
      'uhfp-shortcode-section',
      'Shortcode Settings',
      [$this, 'getShortcodeSettingSection'],
      'uhfp-settings'
    );

    add_settings_section(
      'uhfp-blacklist-section',
      'Blacklist Settings',
      [$this, 'getBlacklistSettingSection'],
      'uhfp-settings'
    );

    add_settings_section(
      'uhfp-clinic-whitelist-section',
      'Clinic Whiltelist Settings',
      [$this, 'getClinicWhitelistSettingSection'],
      'uhfp-settings'
    );
  }

  public function getShortcodeSettingSection() {
    $settings = $this->getSettings();
    $opts = $settings['shortcode-settings'];
    include UHFP2R_DIR . '/partials/admin/shortcode-slug-section.php';
  }

  public function getBlacklistSettingSection(): bool {
    $data = $this->query->getCombinedSpecialties();

    if (is_wp_error($data)) {
      $errors = $data->errors;
      $this->query->displayErrors($errors);
      return false;
    }

    $dental_specs = $data['data']['dentalSpecs'];
    $doctor_specs = $data['data']['doctorSpecs'];
    $settings = $this->getSettings();

    if (!isset($settings['blacklist-settings'])) {
      $settings['blacklist-settings'] = null;
      update_option('uhfp-settings', $settings);
    }

    $opts = $settings['blacklist-settings'];
    include UHFP2R_DIR . '/partials/admin/blacklist-section.php';
    return true;
  }

  public function getClinicWhitelistSettingSection() {
    $settings = $this->getSettings();

    if (!isset($settings['clinic-whitelist-settings'])) {
      $settings['clinic-whitelist-settings'] = null;
      update_option('uhfp-settings', $settings);
    }

    $data = $this->query->getClinicNamesAndLocations();

    usort($data['data']['clinics']['nodes'], [$this, 'sortClinicsByName']);

    include UHFP2R_DIR . '/partials/admin/clinic-whitelist-section.php';

    return true;
  }

  public function getSettings()
  {
    return $this->prepareSettings();
  }

  protected function prepareSettings() {
    if (!get_option('uhfp-settings')) {
      update_option('uhfp-settings', [
        'blacklist-settings' => '',
        'clinic-whitelist-settings' => '',
        'shortcode-settings' => [
          'shortcode-slug' => 'find-provider-2',
          'page-slug' => 'find-a-provider-2'
        ]
      ]);
    }
    return get_option('uhfp-settings');
  }

  protected function sortClinicsByName(array $a, array $b): int {
    return strcmp($a['locationName'], $b['locationName']);
  }

  public function sanitizeSettings($input) {

    echo "<pre>";
    var_dump($input);
    echo "</pre>";

    return $input;
  }
}