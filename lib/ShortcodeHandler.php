<?php

namespace UHFP;

use UHFP\Queries\Queries;

class ShortcodeHandler {

  /**
   * The string for the shortcode
   *
   * @var string
   */
  private $shortcode_slug = 'find-provider';

  /**
   * The slug of the page the shortcode is on
   *
   * @var string
   */
  private $shortcode_page_slug = '';

  public function __construct()
  {
    $shortcode_page = $this->getShortcodePage();
    // get the most recent page to use the shortcode
    // if no page exists, return an empty string so we don't crash the plugin
    $this->shortcode_page_slug = $shortcode_page[count($shortcode_page) - 1]['post_name'] ?? '';
  }

  /**
   * Add the partial with the id for the find a provider application
   *
   * @return string
   */
  public function display(): string {
    $output = ob_start();
    $query = new Queries();
    $data = $query->getCombinedSpecialties();
    if ($data !== null && !is_wp_error($data) && $data['data'] !== null && $data['data']['dentalSpecs'] !== null) {
      $dental_specs = $this->filterSpecialties($data['data']['dentalSpecs']);
      $doctor_specs = $this->filterSpecialties($data['data']['doctorSpecs']);
    }
    include(UHFP2R_DIR . '/partials/public-display.php');
    $output = ob_get_clean();
    return $output;
  }

  /**
   * Filter out specialties that are blacklisted from appearing in the no-js version of the application.
   *
   * @param array $specs - from the query
   * @return array
   */
  public function filterSpecialties(array $specs): array {
    $opts = get_option('uhfp-settings');
    $blacklist = $opts['blacklist-settings'];
    $filtered = array_filter($specs['nodes'], function($spec) use ($blacklist) {
      if (strpos($blacklist, $spec['name']) === false) {
        return $spec;
      }
    });

    $filtered_specs['nodes'] = $filtered;

    return $filtered_specs;
  }

  /**
   * Create a shortcode for the application
   *
   * @return void
   */
  public function addShortcode() {
    add_shortcode($this->shortcode_slug, array($this, 'display'));
  }

  /**
   * Get the current shortcode slug
   *
   * @return string
   */
  public function getShortcodeSlug(): string {
    return $this->shortcode_slug;
  }

  /**
   * Set a new shortcode slug
   *
   * @param string $slug
   * @return void
   */
  public function setShortcodeSlug(string $slug) {
    $this->shortcode_slug = $slug;
  }

  /**
   * Get the page slug the shortcode is meant to be on
   *
   * @return string
   */
  public function getShortcodePageSlug(): string {
    return $this->shortcode_page_slug;
  }

  /**
   * Set the slug of the page for the shortcode
   *
   * @param string $slug
   * @return void
   */
  public function setShortcodePageSlug(string $slug) {
    $this->shortcode_page_slug = $slug;
  }

  /**
   * Get a list of all page slugs where the content includes the [find-provider] shortcode
   *
   * @return array
   */
  private function getShortcodePage(): array
  {
    global $wpdb;

    $query = "SELECT ID, post_name FROM " . $wpdb->posts . " WHERE post_content LIKE '%[" . $this->shortcode_slug . "%' AND post_status = 'publish'";

    $results = $wpdb->get_results($query);

    return array_map(function ($post): array {
      return [
        'ID' => $post->ID,
        'post_name' => $post->post_name
      ];
    }, $results);
  }
}