# Vue Apollo

The `vue-apollo` library is used to manage graphql requests and local data/state management. Because of this, we should not use other state management libraries such as `vuex`.

At some point, the events managed by the `eventBus.js` file might be rolled into apollo and the event bus deprecated. But not right now. I'm going to take these a little out of order because of how they intersect with the vue application.

You'll notice in `app.js` that the only part of apollo imported is the `apolloProvider` so that's where we'll start

## apolloProvider.js
Apollo is accessed at the global level via the apollo provider. [The provider is an object with a variety of options](https://apollo.vuejs.org/api/apollo-provider.html#constructor). We actually keep ours very simple using the defaults and then passing in a custom `apolloClient`. All interactions with apollo in the application will pass through this provider.

## apolloClient.js
The apollo client performs behaviors. For instance, it:
- ensures and manages a client-side cache with the `apolloCache`
- batches graphql requests with `httpLink`
- makes client-side graphql requests with the typeDefs and resolvers

I'll discuss the cache and batched requests in their sections. Let's talk about type definitions and resolvers.

### type definitions
To make client-side requests, graphql needs a way to understand what data should be sent in a request and what data should be returned. To do that, it relies on type definitions. **Important** - These definitions expose what _can_ be returned from a request but not what _will_ be. That's because you might want to give people the ability to get data about 5 properties. But the developer only needs 3 at the moment. That's ok. For instance, the `makeSecureEmailRequest` mutation returns a `SecureEmail` type. That type _can_ return all kinds of things. But in `/mutations/secureEmail.gql` only a few are used.

This file also lets us define custom queries and mutations to extend the root query and mutation. You'll notice the names defined here match the names of the javascript functions in their respective places inside the `resolvers` property of the apollo client.

### resolvers
Speaking of which...

The `resolvers` property is where client-side requests are managed. It has two properties
- `resolvers.Query`
- `resolvers.Muation`
Under these, are the functions which make requests and return data. All resolvers _must_ return something even if it is null although that's kind of a bad idea. Usually it's better to return something like an empty array etc...

## apolloCache.js
This represents the client-side cache. Mostly this can be left alone. However, there is one catch. apollo will want to route requests based on `id` instead of useful things like the provider's `profileId`. Since we don't want routes like `/physician/1234556` we need to be explicit about this. You'll notice that the `dataIdFromObject` function ensures this name change. The `provider` function under `cacheRedirects` allows us to read back the cached name that has been stored in the form `Provider:profileId`.

## apolloHttpLink.js
This file allows us to batch multiple graphql requests into a single request. This way, we save on network requests to the API or any other data source.