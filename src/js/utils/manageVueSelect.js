/**
 * 
 * Returns a value from the vue-select component to bind to data
 * 
 * @param {object} payload 
 * @param {string} property 
 * 
 * @return {string}
 */
export const managePayload = (payload, property) => {
  if (payload && property) {
    return payload !== null ? payload[property] : ''
  }
  return payload !== null ? payload : ''
}