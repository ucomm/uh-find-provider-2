# JS Directory README

Welcome! This is where all the fun stuff happens. You can find more information in the following READMEs.
- [Apollo README](../../src/js/apollo/README.md)
- [Vue Components](../../src/js/Components/README.md)
- [Static Data](../../src/js/data/README.md)
- [GraphQL](../../src/js/graphql/README.md)
- [Utility Functions](../../src/js/utils/README.md)
- [Vendor Support](../../src/js/vendor/README.md)

## app.js
This file is responsible for creating the main vue instance with support for
- a router
- apollo client
- other plugins, filters, and mixins

## eventBus.js
This file creates a secondary/custom vue instance. The only purpose of this instance is to allow us to use custom events which can be passed between components. An example of this behavior can be found in the `AccordionButton` component which emits a variety of events. The main `Accordion` component listens for and manages these events.

## router.js
This file allows us to create routes off the main `/find-a-provider` route. Without this file, we could not make routes to specific specialties, providers, etc...

### lazy loading
In order to reduce the size of the application downloaded by the enduser, the components for each route are lazy loaded on a per route basis. This ensures that if people don't arrive at a route with a single provider's information, for instance, they won't get all the code associated with that view.

### query parameters
Further, route query parameters can be defined in this file. You can see specific examples on the `props` methods of the `/results` or `/physician` routes.

### scroll behavior
When going between one route and the next the default behavior is to keep the window in the same "place". This isn't ideal in our case. You can customize the scroll behavior using the aptly named `scrollBehavior` method. Currently it returns the view to the top of the window on each change.
