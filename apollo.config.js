module.exports = {
  client: {
    service: {
      name: 'find-provider',
      localSchemaFile: './schema.graphql'
    },
    includes: [
      'src/js/**/*.{js,vue,gql}',
    ]
  }
}