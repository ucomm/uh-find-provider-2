/**
 * 
 * Turn a PascalCase string into a camelCase string
 * 
 * @param {string} string a string...
 * 
 * @returns {string} you guessed it. a string.
 */

const makeCamelCase = string => string.charAt(0).toLowerCase() + string.slice(1)

/**
 * 
 * This function creates a new array of objects from a given data set.
 * For a given typename, it will create a new object, add the typename, and modify each property in the data so that it is camelCase.
 * 
 * @param {string} typename - a typename to provide to graphql for each returned object
 * @param {array} data - an array of data objects to transform
 * 
 * @returns {array} 
 */
export const transformNodes = (typename, data) => {
  return data.map(item => {
    const newItem = {
      __typename: typename
    }
    // ensure that all the object properties are camelCase to conform to graphql spec
    for (const property in item) {
      newItem[makeCamelCase(property)] = item[property]
    }
    return newItem
  })
}