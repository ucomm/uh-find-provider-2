# Utilities

This directory contains helper utilities. Usually these are functions I've abstracted and/or refactored to keep the codebase DRY. I've tried to give documentation on these on a per function basis.

I want to describe one or two here for historical reasons

## removeDuplicates.js
The CFAR API doesn't have a way to filter duplicate clinics etc... So that functionality has to be done on the client. This function allows you to reduce an array of objects according to a given property name.

## transformNodes.js
The CFAR API uses PascalCase instead of camelCase. That's fine except when you're working with graphql. The graphql spec only recognizes camelCase. Therefore when you do a client-side query, you have to "fix" the data before it's returned to apollo/graphql.