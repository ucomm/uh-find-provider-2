<?php
/*
Plugin Name: Find a Provider
Description: Vue/Apollo implementation of the Find a Provider application
Author: UComm Web Team
Version: 1.3.7
Text Domain: uh-find-provider
*/

use UHFP\Admin\Options;
use UHFP\Admin\SettingsPage;
use UHFP\ScriptHandler;
use UHFP\ShortcodeHandler;
use UHFP\RewriteHandler;

if (!defined('WPINC')) {
	die;
}

define( 'UHFP2R_DIR', plugin_dir_path(__FILE__) );
define( 'UHFP2R_URL', plugins_url('/', __FILE__) );
define( 'SERVER_NAME', $_SERVER['SERVER_NAME'] );
define( 'HTTP_SCHEME', isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https://' : 'http://');

// select the right composer autoload.php file depending on environment.
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
	require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
	require_once(ABSPATH . 'vendor/autoload.php');
} else {
	require_once('vendor/autoload.php');
}

require 'lib/Queries.php';
require 'lib/Admin/Options.php';
require 'lib/Admin/SettingsPage.php';
require 'lib/ScriptHandler.php';
require 'lib/ShortcodeHandler.php';
require 'lib/RewriteHandler.php';

$settings_page = new SettingsPage();
$settings_page->init();

$options = new Options();
$options->init();

$settings = $options->getSettings();
$shortcode_slug = $settings['shortcode-settings']['shortcode-slug'];
$shortcode_page_slug = $settings['shortcode-settings']['page-slug'];

$shortcode = new ShortcodeHandler();
$shortcode->setShortcodeSlug($shortcode_slug);
$shortcode->setShortcodePageSlug($shortcode_page_slug);
$shortcode->addShortcode();

new RewriteHandler($shortcode->getShortcodePageSlug());

$scripts = new ScriptHandler($shortcode->getShortcodePageSlug());
$scripts->enqueue_public_scripts();
$scripts->enqueue_admin_scripts();

if (is_admin() && isset($_POST['clear-fap2-cache']) && $_POST['clear-fap2-cache'] === "true") {
	if (function_exists('w3tc_flush_all')) {
		w3tc_flush_all();
		\UConnHealth\CacheManager::clearCache(true);
	}
} 


// add_action('w3tc_flush_all', function() {

// 	\UConnHealth\CacheManager::clearCache(true);

//   });