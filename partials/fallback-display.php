<form action="" method="POST" name="find-provider-compat">
  <div class="form-group">
    <div>
      <label for="compat-specialties[dentist]">Dental Specialties</label>
      <select class="form-control" name="compat-specialties[dentist]" multiple size="10">
        <?php
        if (isset($dental_specs['nodes']) && count($dental_specs['nodes']) > 0) {
          foreach ($dental_specs['nodes'] as $key => $spec) {
            include UHFP2R_DIR . '/partials/no-js/option.php';
          }
        }
        ?>
      </select>
    </div>
    <div>
      <label for="compat-specialties[doctor]">Doctor Specialties</label>
      <select class="form-control" name="compat-specialties[doctor]" multiple size="10">
        <?php
        if (isset($doctor_specs['nodes']) && count($doctor_specs['nodes']) > 0) {
          foreach ($doctor_specs['nodes'] as $key => $spec) {
            include UHFP2R_DIR . '/partials/no-js/option.php';
          }
        }
        ?>
      </select>
    </div>
  </div>
  <button class="btn btn-health" type="submit">Submit</button>
</form>
<?php

if (isset($_POST['compat-specialties']['dentist'])) {
  $specs = $_POST['compat-specialties'];
  $result = $query->getProvidersBySpecialties('DENTIST', $specs);
}

if (isset($_POST['compat-specialties']['doctor'])) {
  $specs = $_POST['compat-specialties'];
  $result = $query->getProvidersBySpecialties('DOCTOR', $specs);
}
?>
<div class="table-responsive provider-table">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Photo</th>
        <th>Provider</th>
        <th>Contact</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!is_wp_error($result) && count($result['data']['providers']['nodes'])) {
        foreach ($result['data']['providers']['nodes'] as $provider) {
          include UHFP2R_DIR . '/partials/no-js/table-info.php';
        }
      } else if (is_wp_error($result)) {
        $query->displayErrors($result->errors);
      }
      ?>
    </tbody>
  </table>
</div>