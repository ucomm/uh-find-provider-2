import Vue from 'vue'
import VueApollo from 'vue-apollo'
import VueRouter from 'vue-router'

import apolloProvider from './apollo/apolloProvider'
import router from './router'
import FindProvider from './Components/FindProvider'

// allow devtools on dev and test sites for debugging
if (window.location.host !== 'health.uconn.edu') {
  Vue.config.devtools = true
}

Vue.use(VueRouter)
Vue.use(VueApollo)

const trim = val => {
  if (val) {
    return val.toString().trim()
  }
}

Vue.filter('trim', trim)

/**
 * 
 * Some specialties like ortho/sports medicine take the form "Specialty Name - SubSpecialty"
 * This filter tests for multiple groups. 
 * If multiple groups are found, it returns the last one
 * 
 * @param {string} specialty - the specialty to filter
 * @returns {string} specialty suffix
 * 
 */
const filterSpecialty = specialty => {
  if (specialty) {
    // match all regex word characters, the "&", "'", "," "/" symbols and spaces
    const regex = /[\w&',\/ \-]+/g
    const found = specialty.match(regex)
    return found.length > 1 ? found[found.length - 1] : found[0]
  }
}

Vue.filter('filterSpecialty', filterSpecialty)

Vue.mixin({
  methods: {
    /**
     *
     * Create a provider's full name from the parameters.
     * For the full provider directory, providers are listed in alphabetical order.
     * The last argument will return this version of their name
     *
     * @param {string} firstName
     * @param {string} middleName
     * @param {string} lastName
     * @param {string} credentials
     * @param {boolean} [isAlphaOrder = false]
     * @returns {string} the provider's full name
     */
    fullName(firstName, middleName, lastName, credentials, isAlphaOrder = false) {
      const first = firstName.trim()

      let middle = middleName !== null && middleName.length >= 1 ?
        ` ${middleName.trim()[0]}.` : ''
      if (!isAlphaOrder) {
        middle += ' '
      }

      const last = lastName.trim()
      const creds = credentials !== undefined && credentials !== '' ?
        credentials.trim() : ''

      return isAlphaOrder ? 
        `${last}, ${first}${middle}, ${creds}` :
        `${first}${middle}${last}, ${creds}`
    }
  }
})

const vue = new Vue({
  el: '#find-a-provider',
  components: {
    'find-provider': FindProvider
  },
  router,
  apolloProvider,
  render: h => h('find-provider')
})

export default vue