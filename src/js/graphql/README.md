# GraphQL

## Introduction

### Basics
GraphQL requests are different from REST requests. All GraphQL requests are made to the same endpoint `/graphql`. REST endpoints represent resources but doesn't tell you anything about data. GraphQL requests in contrast tell you about data and data relationships, but don't necessarily expose how that data is fetched.

For instance, if you make a `GET` request to `https://example.com/wp-json/wp/v2/posts` you can expect to get a variety of posts, but you won't know what kinds of data are coming back. If you make a `graphql` query to something like `https://example.com/graphql` and ask for the following data, you'll know exactly what will come back

```gql
posts {
  edges {
    node {
      title
      author {
        name
      }
      content
    }
  }
}
```
For every post, you will get the title, author's name, and the post content.

### GraphQL files
This directory contains `.gql` files. These are separated from the components which use them. This way, if the components or even the whole framework (react or svelte instead of vue) needs to be changed, we don't need to touch these files. The requests will still be the same.

These files are `require`d by apollo components `<ApolloQuery>` or `<ApolloMutation`>. They can also be used in vue-apollo smart queries or the `$apollo` property on a component instance.

### GraphQL Conventions
GraphQL understands 5 scalar types: 
- Boolean
- Float
- ID
- Int
- String

Other types such as `enum`s can be defined (See `ProviderCategoryEnum` for an example). In addition, you can combine these types to create data schemas. See below under Types.

Other symbols
- `!` a required paramter. For instance to require a string use `String!`
- `[]` a list. For instance a list of strings `[String]`
- These can be combined
    - `[String]!` a required list with optional strings
    - `[String!]` an optional list where if it exists, strings are required
- `@client` This is a special decorator for apollo. It tells apollo that the associated request will be performed by the browser instead of a server. [Read more about local state in the apollo documentation.](https://www.apollographql.com/docs/react/data/local-state/)

## Mutations
A `mutation` in graphql represents a request that performs the C, U, or D of a CRUD app. There are only two in this application:
- routeParams.gql
- secureEmail.gql

### Syntax
The `gql` mutation files have the following syntax
```
# the mutation keyword is required. otherwise the action is assumed to be a query
mutation MUTATION_NAME (
  $variable1: {Graphql Type}
  $variableN: {Graphql Type}
) {
  mutationFunctionName (
    input: {
      variable1: $variable1
      variableN: $variableN
    }
  ) (optional @client directive) {
    # return values
    __typename
    value1
    valueN
  }
}
```
### routeParams.gql
This mutation helps us store route paramters in order to perform re-fetch data faster from the apollo cache. If someone searches for doctors in Hartford, then searches for doctors in West Hartford, each search takes a little time to complete. However, if they then search for doctors in Hartford, the search is instantaneous.

### secureEmail.gql
This mutation will send patient data from the appointment request form to the UCHC secure email application. It enforces that some fields (like first name or phone number) are required.

## Queries
The majority of the `gql` files in this application are queries because CFAR API is read-only. Queries represt `READ` operations in a CRUD app even though they are performed with a `POST` request.

### Syntax
The syntax for these files is similar to mutations. However instead of having an `input` area, you will see a `where` area. This acts as a filter on the query. The `where` filter is optional in queries though since it's possible to make a request without any filters.

Instead of having a function name after the opening curly braces, you will find a field or connection name (e.g. clinic or providers).

```
query QUERY_NAME (
  $variable1: {GraphQL Type}
  $variableN: {GraphQL Type}
) {
  fieldName (
    where: {
      variableName1: $variable1
      variabelNameN: $variableN
    }
  ) {
    # returned data
  }
}
```

## Types
The majority of types for this application are defined on the server with the `uh-find-provider-graphql` plugin. However, in order to ensure proper client-side behavior, some types are defined in the `typeDefs.gql` file. This file determines what types, queries, and mutations the client "knows" about. This file is imported directly into the apollo client.