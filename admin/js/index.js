const state = {
  blacklistString: null,
  whitelistString: null,
  blacklistArr: [],
  whitelistClinicsArr: []
}

const specialtySelect = document.querySelector('#find-provider-specialties');
const blacklistTextArea = document.querySelector('#uhfp-blacklist')
const addSpecialty = document.querySelector('#add-specialty')
const removeAll = document.querySelector('#remove-all')
const blacklistItemList = document.querySelector('#blacklist-item-list')

const whitelistTextArea = document.querySelector('#uhfp-whitelist')
const clinicSelect = document.querySelector('#add-clinic-select')
const addClinicButton = document.querySelector('#add-clinic-button')
const addAllCinics = document.querySelector('#add-all-clinic-button')
const removeAllClinics = document.querySelector('#remove-all-clinic-button')
const whitelistItemList = document.querySelector('#whitelist-clinic-list')

const listFilters = document.querySelectorAll('#uhfp-settings-form .uhfp-filter')

const createSingleListItem = (type, data) => {
  const item = document.createElement('li')
  const id = data.phone !== undefined ? 
    btoa(data.text + data.phone) :
    btoa(data.text)
  item.setAttribute('id', `${type}-list-item-${id}`)

  const itemInfo = data.phone !== undefined ?
    `<p class='info-text'><span>${data.text}</span></p>
     <p class='info-phone'><span>${data.phone}</span></p>` :
    `<p class='info-text'><span>${data.text}</span></p>`

  item.innerHTML = `<div>
      <div class='info-container'>
        ${itemInfo}
      </div>
      <button id='${type}-remove-${btoa(id)}' class='button-red'>Remove</button>
    </div>`

  return item.outerHTML
}

const createMultipleListItems = (type, arr) => {
  return arr.map(item => {
    return createSingleListItem(type, item)
  }).join('')
}

const removeListItem = (target, arr) => {
  const textToFind = target.closest('div').querySelector('.info-text').innerText
  const foundIndex = arr.findIndex(item => item.text === textToFind)
  return arr.slice(0, foundIndex).concat(arr.slice(foundIndex + 1))
}

const updateList = (type, textArea, itemList) => {
  const stateArray = type === 'clinic' ? state.whitelistClinicsArr : state.blacklistArr
  const stateString = type === 'clinic' ? state.whitelistString : state.blacklistString
  const listItems = createMultipleListItems(type, stateArray)
  textArea.value = stateString
  itemList.innerHTML = listItems
}

const createSelectedData = (selector, arr) => {
  return Array.from(selector).filter(opt => {
      if (arr.length === 0) {
        return opt
      }

      const foundInState = arr.findIndex(spec => spec.text === opt.value)

      if (foundInState === -1) {
        return opt
      }
    }).map(opt => {
      const data = { text: opt.value }
      if (opt.getAttribute('data-phone') !== null) {
        data.phone = opt.getAttribute('data-phone')
      }
      return data
    })
}

const sortData = (arr, selectedArr) => {
  return arr.concat(selectedArr).sort((a, b) => {
    const textA = a.text.toLowerCase()
    const textB = b.text.toLowerCase()

    return textA > textB ? 1 : -1
  })
}

const createStateString = (arr) => arr.map(({ text }) => text).join('+ ')

const filterList = ({ target, which }) => {
  const listContainer = target.parentNode.nextElementSibling
  const listItems = Array.from(listContainer.querySelectorAll('ul li'))

  if (which === 27 || target.value === '') {
    target.value = ''
    listItems.map(item => {
      item.style.display = 'block'
      return item
    })
  }

  if (target.value.length > 2) {
    listItems.map((item) => {
      const text = item.innerText.toLowerCase()
      const search = target.value.toLowerCase()
      if (!text.includes(search)) {
        item.style.display = 'none'
        return item
      }
    })
  }
}

document.addEventListener('DOMContentLoaded', () => {
  // get the values from the db and initialize the global state   
  const newState = {}
  const clinicOptions = Array.from(clinicSelect.querySelectorAll('option')).map(opt => {
    return {
      phone: opt.getAttribute('data-phone'),
      value: opt.value
    }
  }).filter(value => {
    if (value !== undefined) {
      return value
    }
  })

  if (adminOptions['blacklist-settings'] !== null && adminOptions['blacklist-settings'] !== '') {

    const specsArray = adminOptions['blacklist-settings'].split('+ ').map(function(item) {
      return { text: item }
    })

    Object.assign(newState, {
      blacklistString: adminOptions['blacklist-settings'],
      blacklistArr: specsArray,
    })
  }
  
  if (adminOptions['clinic-whitelist-settings'] !== null && adminOptions['clinic-whitelist-settings'] !== '') {
    
    const clinicsArray = adminOptions['clinic-whitelist-settings'].split('+ ').map(function(item) {
      const found = this.find(opt => opt.value === item)
      return {
        text: item,
        phone: found.phone
      }
    }, clinicOptions)
    
    Object.assign(newState, {
      whitelistString: adminOptions['clinic-whitelist-settings'],
      whitelistClinicsArr: clinicsArray
    })
  }

  Object.assign(state, newState)
  
  blacklistTextArea.value = state.blacklistString
  whitelistTextArea.value = state.whitelistString
  
  Array.from(specialtySelect.querySelectorAll('option')).map(function(opt) {
    if (this.indexOf(opt.value) > -1) {
      opt.selected = true
    }
    return opt
  }, state.blacklistArr)
  
  updateList('specialty', blacklistTextArea, blacklistItemList)
  updateList('clinic', whitelistTextArea, whitelistItemList)
})

addSpecialty.addEventListener('click', (evt) => {
  evt.preventDefault()

  const selected = createSelectedData(
    specialtySelect.querySelectorAll('option:checked'),
    state.blacklistArr
  )

  if (!selected.length) {
    return
  }

  const newSpecialtyList = sortData(state.blacklistArr, selected)
  const newSpecialtyString = createStateString(newSpecialtyList)
  
  Object.assign(state, {
    blacklistArr: newSpecialtyList,
    blacklistString: newSpecialtyString
  })

  updateList('specialty', blacklistTextArea, blacklistItemList)
})

removeAll.addEventListener('click', (evt) => {
  evt.preventDefault()
  blacklistTextArea.value = ''
  Object.assign(state, { blacklistArr: [], blacklistString: '' })
  blacklistItemList.innerHTML = ''
})

removeAllClinics.addEventListener('click', (evt) => {
  evt.preventDefault()
  whitelistTextArea.value = ''
  Object.assign(state, { whitelistClinicsArr: [], whitelistString: '' })
  whitelistItemList.innerHTML = ''
})

blacklistItemList.addEventListener('click', evt => {
  if (evt.target.localName !== 'button') {
    return
  }

  evt.preventDefault()

  const blacklistArr = removeListItem(evt.target, state.blacklistArr)
  const blacklistString = createStateString(blacklistArr)

  Object.assign(state, {
    blacklistArr,
    blacklistString
  })

  updateList('specialty', blacklistTextArea, blacklistItemList)
})

addClinicButton.addEventListener('click', (evt) => {
  evt.preventDefault()

  const selected = createSelectedData(
    clinicSelect.querySelectorAll('option:checked'), 
    state.whitelistClinicsArr
  )

  if (!selected.length) {
    return
  }

  const newWhitelist = sortData(state.whitelistClinicsArr, selected)
  const newWhitelistString = createStateString(newWhitelist)

  Object.assign(state, {
    whitelistClinicsArr: newWhitelist,
    whitelistString: newWhitelistString
  })

  updateList('clinic', whitelistTextArea, whitelistItemList)
})

addAllCinics.addEventListener('click', (evt) => {
  evt.preventDefault()

  const clinics = createSelectedData(
    clinicSelect.querySelectorAll('option'), 
    state.whitelistClinicsArr
  )

  const newWhitelist = sortData(state.whitelistClinicsArr, clinics)

  const newWhitelistString = createStateString(newWhitelist)
  Object.assign(state, {
    whitelistClinicsArr: newWhitelist,
    whitelistString: newWhitelistString
  })

  updateList('clinic', whitelistTextArea, whitelistItemList)
})

whitelistItemList.addEventListener('click', (evt) => {
  if (evt.target.localName !== 'button') {
    return
  }

  evt.preventDefault()

  const newWhitelist = removeListItem(evt.target, state.whitelistClinicsArr)
  const newWhitelistString = createStateString(newWhitelist)

  Object.assign(state, {
    whitelistClinicsArr: newWhitelist,
    whitelistString: newWhitelistString
  })

  updateList('clinic', whitelistTextArea, whitelistItemList)
})

listFilters.forEach((filter) => {
  filter.addEventListener('keyup', (evt) => filterList(evt))
})