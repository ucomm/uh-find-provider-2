# The Vendor Directory

This directory contains files meant to support functionality from specific vendors. Breaking these vendors out like this makes lazy loading easier to manage. 

## vueTelInput.js
This is just ridiculous. The [vue-tel-input dependency](https://educationlink.github.io/vue-tel-input/) is larger in size than vue, apollo, and most of the components put together. So we only want to load it in the event that it's _absolutely_ needed. In this case that means only when a user arrives at the appointment request form for a single provider. We should get rid of this. I'd be amazed if anyone uses it. See the `RequestAppointmentForm` component at `src/js/Components/Forms/RequestAppointmentForm.vue` for implementation.

## vueYouTube.js
This file lets us load youtube videos only on single provider pages. Youtube isn't used anywhere else in the application. Therefore, there's no reason to load it anywhere else. [Full documentation is on github](https://github.com/kaorun343/vue-youtube-embed). See the `YouTube` component in `/src/js/Components/SingleProvider/YouTube.vue` for implementation. 