#!/bin/bash

# you may need to install npm deps first and then run other tasks.

npm set progress=false

# npm cache clean --force

npm install
npm audit fix
# npm run gulp
npm run gulp:watch