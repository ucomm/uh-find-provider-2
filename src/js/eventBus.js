import Vue from 'vue'

// create a custom global event bus.
// this allows us to emit and listen to events across components
const eventBus = new Vue()
export default eventBus