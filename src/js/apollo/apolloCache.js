import { InMemoryCache, defaultDataIdFromObject } from 'apollo-cache-inmemory'

const cache = new InMemoryCache({
  dataIdFromObject: object => {
    // give providers a unique id based on their profileID.
    // this will be referenced to speed up the tranisition to the single provider page
    // by accessing the cached data from Apollo
    switch (object.__typename) {
      case 'Provider':
        return `Provider:${object.profileId}`
      case 'SecureEmailToken':
        return `UHToken:${object.id}`
      default:
        return defaultDataIdFromObject(object)
    }
  },
  cacheRedirects: {
    Query: {
      provider: (_, args, { getCacheKey }) => {
        // get the cached data based on the unique ID stored in the cache
        return getCacheKey({ __typename: 'Provider', profileId: args.profileId })
      },
    }
  }
})

export default cache