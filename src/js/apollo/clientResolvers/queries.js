import { get } from 'axios'

import { transformNodes } from '../../utils/transformNodes'

const baseUrl = 'https://publicdirectoryapi.uchc.edu/api'

/**
 * 
 * @param {string} category the type of clinic to query about
 * 
 * @returns {object} an object with a typename of ClinicCollection and an array of clinic nodes
 */
export const clinicsQuery = async (category) => {
  let url = `${baseUrl}/Providers/clinics`
  if (category !== '') {
    url += `?category=${category}`
  }
  return await get(url).then(({ data }) => {
    const nodes = transformNodes('Clinic', data)
    return {
      __typename: 'ClinicCollection',
      nodes
    }
  }).catch(err => console.error(err, 'axios error'))
}

/**
 *
 * @param {string} category the type of specialty to query about
 *
 * @returns {object} an object with a typename of SpecialtyCollection and an array of specialty nodes
 */
export const specialtiesQuery = async (category) => {
  const url = `${baseUrl}/Providers/${category.toLowerCase()}/specialties`

  return await get(url).then(({ data }) => {
    const nodes = transformNodes('Specialty', data).filter(node => {
      if (node.isVisible) {
        return node
      }
    })
    return {
      __typename: 'SpecialtyCollection',
      nodes
    }
  }).catch(err => console.error(err, 'axios error'))
}  