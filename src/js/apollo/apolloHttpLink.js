import { BatchHttpLink } from 'apollo-link-batch-http'
import fetch from 'unfetch'

let uri = window.location.origin

if (uri.includes('localhost')) {
  uri += '/wordpress'
}

uri += '/graphql'

const httpLink = new BatchHttpLink({ uri, fetch })

export default httpLink