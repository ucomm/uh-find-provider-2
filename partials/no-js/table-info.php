<tr>
  <td width="180">
    <img src="https://files-universityofconn.netdna-ssl.com/shared/health/provider-images/<?php echo $provider['profileId']; ?>.png" alt="<?php echo $provider['firstName'] . ' ' . $provider['lastName'] . ' ' . $provider['credentials']; ?>">
  </td>
  <td>
    <p><strong><?php echo $provider['firstName'] . ' ' . $provider['lastName']; ?></strong></p>
    <p>Specialties</p>
    <ul>
      <?php
      foreach ($provider['specialties']['nodes'] as $key => $spec) {
      ?>
        <li><?php echo $spec['name']; ?></li>
      <?php
      }
      ?>
    </ul>
  </td>
  <td>
    <p>Call <a href="tel:18443882666">1-84-GET-UCONN</a> (1-844-388-2666).</p>
  </td>
</tr>