export const setInitialElement = (elements, option) => {
  const options = elements.map(el => el.option)
  const useOption = options.includes(option)

  if (!useOption) {
    return elements[0]
  } else {
    return elements.find(el => el = option === el.option)
  }
}

export const setActiveElements = (elements, elementToCheck) => {
  return elements.map(el => {
    el.isActive = false
    el.isSelected = false
    if (el === elementToCheck) {
      el.isActive = true
      el.isSelected = true
    }
    return el
  })
}