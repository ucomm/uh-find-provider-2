# Find a Provider Application Plugin

## Description
This is a plugin developed for UConn Health. It provides the following features:

- **Javascript application:** built with vue, apollo client for graphql support, webpack for code splitting and lazy loading parts of the application. The JS application is compatable with modern browsers and IE11. 
- **No-JS application:** basically a PHP form that makes a graphql request and receives responses. It's included just to cover a case where someone wants to find a provider but doesn't want to use javascript.
- **Admin settings:** the settings include support for changing the shortcode with which the app is instantiated, the page slug the app should be found on, a blacklist of supported specialties, a whitelist of clinics/phonenumbers.

## Project Structure

**Important** - in order to not make this file enormous, you will find additional READMEs throughout the project to explain particular functionality in more detail.

### Files
- **.babelcrc**: babel js configuration is done from here. Currently includes support for: 
    - webpack code splitting
    - dynamic polyfills
    - vue
- **.browserslistrc**: defines the browsers the project will support. Modifying this file will change the polyfills used by core-js
- **apollo.config.js**: a very barebones apollo config. Helps suppress some warnings occasionally. There are probably _a lot_ of other things you can do with it. 
- **composer.json**: defines PHP dependencies
- **composer.lock**: locks PHP dependencies to versions so we don't get version conflicts between environments
- **docker-compose.yml**: defines the virtual environment to run the development application
- **gulpfile.js**: defines tasks to transpile frontend source files
- **package-lock.json**: locks versions for js dependencies
- **package.json**: defines js dependenices. also used to kickoff gulp tasks. Cross env support is defined here as well
- **README.md**: this file
- **schema.graphql**: used by apollo.config.js to imporve IDE support
- **uh-find-provider-2.php**: the main wordpress plugin file. PHP functions are started from here. also defines constants, requires files, and initializes classes
- **webpack.common.js**:  (mostly) contains config settings that are  used in both development and production. Ensures that built files are named correctly and have the correct `publicPath` which allows them to be found
- **wepack.dev.js**: dev environment configuration should go here
- **webpack.prod.js**: prod environment configuration should go here

### Directories
- **/admin**: admin side js and css files. These files are currently _not_ processed by gulp or webpack.
- **/build**:  assets built by gulp/webpack. This directory is gitignored. In development, it will include a file that can be served from `/build/js/report/report.- html`. This will show a graphical representation of how webpack code splits the application.
- **/lib**: PHP classes needed to run the application. 
- **/partials**: PHP presentation files. These contain very little logic. The no-JS and IE > 11 applications are built up from [/partials/fallback-display.php](/partials/fallback-display.php) and [/partials/no-js/table-info.php](/partials/no-js/table-info.php)
- **/scripts**: Not really used in this application.
- **/src**: The js application source and sass styles
- **/src/js**: This is where the vue application is developed. It is probably the most complex part of the plugin. The main parts are
    - **/apollo** which handles the apollo client configuration files. These are used to manage graphql requests and the client cache
    - **/Components** stores vue components
    - **/data** should hold any static data file in json format
    - **/graphql** holds graphql types, queries, and mutations which are used by vue/apollo
    - **/utils** has utility functions
    - **/vendor** specific instances of vue that can be used for vendor specific libraries that should not be globaly available. These files aid in code splitting.     See the `vueTelInput.js` file as an example`
    - **app.js** the main vue application. Responsible for loading the other files correctly
    - **eventBus.js** a separate vue instance used to provide custom event types for the accordions and tabs
    - **router.js** defines the routes and query params supported by the application. Also lazy loads components based on the route
- **/src/sass**: Styles which will be turned into CSS by gulp. [main.scss](/src/sass/main.scss) contains style support for the `vue-select` component

## Development

### Starting off
To work on this application
- clone the repo
- `composer install`
- `docker-compose up` this also installs js dependencies

In the admin area, make sure the following plugins are activated
- WPGraphQL
- Find a Provider Graphql Interface
- This plugin

Without the first two plugins, the application will not run correctly (or really at all). 

You can use a graphql sandbox app like [GraphiQL](https://github.com/graphql/graphiql) or [GraphQL Playground](https://github.com/prisma-labs/graphql-playground) to experiment with queries. It can also be helpful to use something like [Postman](https://www.postman.com/downloads/) to make HTTP requests directly to the graphql endpoint `/wordpress/graphql`.

### Initial setup
After you activate the plugin, you'll need to create a page with the slug `find-a-provider-2`. The easiest way to do this is to create a page called **Find a Provider 2**. Next, add a shortcode to it like this -> `[find-provider-2]`. If you want to use a different page slug/shortcode slug, you may. You can change these in the admin settings area.

### Debugging
Install and use [Vue DevTools](https://github.com/vuejs/vue-devtools) in the browser(s) of your choice. You may also want to install [Apollo Dev Tools](https://github.com/apollographql/apollo-client-devtools). However, I believe they only work in Chrome.

## Known Issues
- Occasionally over a slow connection, the request appointment form takes a moment to load. This is because the library that supports international phone numbers is _ridiculously large_. It is intentionally split from the rest of the code. But maybe there's a better way to do it?

