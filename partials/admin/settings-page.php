<?php

if (!current_user_can('manage_options')) return;

$cached_transients = \UConnHealth\CacheManager::getWPCachedTransients()
?>

<div class="wrap">
  <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
  <form id="uhfp-settings-form" action="options.php" method="POST">
    <?php

      settings_fields('uhfp-settings');

      do_settings_sections('uhfp-settings');

      submit_button('Update Settings');
    ?>
  </form>
  <div>
    <h2>Clear Find-a-Provider-2 Cache</h2>
    <p>The button below can be used to clear the cached responses for the Find-a-Provider-2 plugin.</p>  
    <form action="" method="POST" style="padding-bottom: 20px;">
      <input 
          type="hidden" 
          name="clear-fap2-cache" 
          id="clear-fap2-cache" 
          value="true" />
      <input 
          type="submit" 
          id="clear-fap2-cache-submit"
          class="button button-primary" 
          value="Clear Provider Cache" />
  </form>
    <span style="font-weight: 700; text-decoration: underline;">Currently Cached Transients:</span>
    <ul>
    <?php foreach ($cached_transients as $transient){ 
      if (($transient->option_name != "_transient_fap-provider_dental_specs") and ($transient->option_name != "_transient_fap-provider_specs")){ ?>
        <li><?php echo $transient->option_name; ?> </li>
      <?php
      }
      } ?>
    </ul>
            
  </div>
  
</div>