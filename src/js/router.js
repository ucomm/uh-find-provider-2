import VueRouter from 'vue-router'

/**
 * 
 * Dynamically import components to reduce the total size initially fetched by the application
 * Without this, the entire app bundle is fetched whether or not it's needed.
 * See vue router documentation on lazy loading routes - https://router.vuejs.org/guide/advanced/lazy-loading.html
 * 
 */
const FindProviderSearch = () => import(/* webpackChunkName: "uhfp-search" */  './Components/FindProviderSearch')
const ProviderResults = () => import(/* webpackChunkName: "uhfp-results" */  './Components/ProviderResults')
const SingleProvider = () => import(/* webpackChunkName: "uhfp-single" */ './Components/SingleProvider')

const router = new VueRouter({
  // history mode makes sure there are no # marks in the url
  mode: 'history',
  routes: [
    {
      path: '/',
      alias: '/search',
      component: FindProviderSearch,
      props: (route) => ({ option: route.query.option })
    },
    {
      path: '/results',
      // you can only pass params on routes with a name.
      name: 'results',
      component: ProviderResults,
      // add possible query params to the results route
      // this will let us link directly to a specific set of results
      props: (route) => ({
        category: route.query.category,
        lastName: route.query.lastName,
        location: route.query.location,
        specialty: route.query.specialty
      }),
    },
    {
      // pass the profileId of a provider to the component when it's rendered
      path: '/physician/:profileId',
      name: 'physician',
      component: SingleProvider,
      props: (route) => ({ email: route.query.email })
    }
  ],
  /**
   * 
   * the view of each route navigation will return to the top of the page
   * see - https://router.vuejs.org/guide/advanced/scroll-behavior.html
   * 
   */
  scrollBehavior () {
    return { x: 0, y: 0}
  }
})

export default router