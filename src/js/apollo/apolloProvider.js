import VueApollo from 'vue-apollo'
import apolloClient from './apolloClient'

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

export default apolloProvider