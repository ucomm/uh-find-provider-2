<div>
  <h3>Shortcode Slug Setting</h3>
  <p>Use this setting to update the value what the shortcode slug should be. The default is <code>find-provider</code>.</p>
  <div>
    <label for="uhfp-shortcode-slug">Shortcode Slug</label>
    <input type="text" name="uhfp-settings[shortcode-settings][shortcode-slug]" id="uhfp-shortcode-slug" value="<?php echo $opts['shortcode-slug']; ?>" placeholder="shortcode-slug" />
  </div>
</div>

<div>
  <h3>Shortcode Page Slug Setting</h3>
  <p>Use this setting to update which <em>page slug</em> has the shortcode. The default is <code>find-a-provider</code>.</p>
  <div>
    <label for="uhfp-shortcode-page-slug">Shortcode Slug</label>
    <input type="text" name="uhfp-settings[shortcode-settings][page-slug]" id="uhfp-shortcode-page-slug" value="<?php echo $opts['page-slug']; ?>" placeholder="shortcode-slug" />
  </div>
</div>