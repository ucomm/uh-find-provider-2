<?php

namespace UHFP;

use UHFP\Admin\Options;

class ScriptHandler {
  public $slug = '';
  public $options;
  public function __construct(string $slug)
  { 
    $options = new Options();
    $this->options = $options->getSettings();
    $this->slug = $slug;
  }

  /**
   * Enqueue plugin scripts and styles only on pages with the shortcode
   *
   * @return void
   */
  public function prepare_scripts() {
    if (is_page($this->slug)) {
      $options = [
        'blacklistSettings' => $this->options['blacklist-settings'],
        'whitelistSettings' => $this->options['clinic-whitelist-settings']
      ];
      wp_register_script('find-provider-commons', UHFP2R_URL . 'build/js/commons.bundle.js', array(), false, true);
      wp_register_script('find-provider', UHFP2R_URL . 'build/js/main.bundle.js', ['find-provider-commons'], false, true);
      wp_localize_script('find-provider', 'adminOptions', $options);
      wp_enqueue_script('find-provider');
      wp_enqueue_style('find-provider', UHFP2R_URL . 'build/css/main.css');
    }
  }

  public function prepare_admin_scripts($hook_suffix) {
    if ($hook_suffix === 'settings_page_uhfp-settings') {
      wp_register_script('find-provider-2-admin', UHFP2R_URL . 'admin/js/index.js', [], false, true);
      wp_localize_script('find-provider-2-admin', 'adminOptions', $this->options);
      wp_enqueue_script('find-provider-2-admin');
      wp_enqueue_style('find-provider-2-admin', UHFP2R_URL . 'admin/css/style.css');
    }
  }

  /**
   * Get the enqueued scripts onto the page
   *
   * @return void
   */
  public function enqueue_public_scripts() {
    add_action('wp_enqueue_scripts', [$this, 'prepare_scripts']);
  }

  public function enqueue_admin_scripts() {
    add_action('admin_enqueue_scripts', [$this, 'prepare_admin_scripts']);
  }
}
