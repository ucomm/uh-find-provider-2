# Changelog
## 1.3.7
- Well... it turns out the last release had a lot of bugs in the admin area. So I refactored it to eliminate them. This version should be a lot better.

## 1.3.6
- Enhanced the admin area settings to handle displaying phone numbers for clinics
- Removed the `vue-fragment` dependency. It was causing unexpected problems and breaking parts of the app.

## 1.3.5
- Various small tweaks for formatting 

## 1.3.4
- Provider's middle initials now display in all relevant views
- View Bio button appears in results for providers who are not currently accepting patients.

## 1.3.3
- Better full provider directory
- Fixed bug with office locations accordion panel
- Better handling of provider images which aren't fetched

## 1.3.2
- Added zip code with browser validation to appointment form

## 1.3.1
- Adjusted results layout box to switch the order of the locations and accepting/not accepting text
- Improved UX for users who arrive at a single provider page
- Added additional loading text to results page
- Style adjustments

## 1.3.0
- Improved select/search box
- Updated results layout
- Added additional request appointment support
    - added fields
    - adjusted mutation
    - added type support for graphql

## 1.2.1
- Fixed tab button responsive styling

## 1.2.0
- Improved in app navigation with browser back/forward controls
- Improved layout of results page
- Default CTA on provider results is routed to the appointment request form
- Improved color contrast with new health teal brand color

## 1.1.0
- Added honeypot fields to the appointment request form
- Using uchc secure email application to make appointments
- Support for client only secure email requests. This fix bypasses requests to the Health Aurora server for appointment requests.

## 1.0.1
- Fixed formatting issue with provider education component
- Updated `adminOptions` so that they initialize with strings instead of nulls

## 1.0.0
- Initial release