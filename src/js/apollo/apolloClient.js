import { ApolloClient } from 'apollo-client'
import cache from './apolloCache'
import httpLink from './apolloHttpLink'

import { modifyRequestApptData, postSecureData } from '../utils/formSubmission'
import { clinicsQuery, specialtiesQuery } from './clientResolvers/queries'

/**
 * 
 * Apollo needs type definitions to make client side requests
 * 
 */
const typeDefs = require('../graphql/types/typeDefs.gql')

const apolloClient = new ApolloClient({
  cache,
  link: httpLink,
  typeDefs,
  resolvers: {
    Query: {
      /**
       * 
       * Similar to the specialties query, it is more efficient to perform this request here.
       * 
       * @param {object} where - a clinic type of ALL, PRIMARY, SPECIALTY
       * @returns {object} - clinic nodes and typename
       * 
       */
      clinics: async (_, { where }) => {
        return await clinicsQuery(where.category)
      },
      /**
       * 
       * Making a request for provider specialties on the server takes a really long time.
       * Sometimes close to 10sec.
       * Making the request on the client reduces that time to under 1sec.
       * 
       * @param {object} where - the category of specialty to retrieve
       * @returns {object} - the nodes and a typename 
       * 
       */
      specialties: async (_, { where }) => {
        return await specialtiesQuery(where.category)
      }
    },
    Mutation: {
      /**
       * A client only request to the UConn Health secure email application
       * We don't want any patient data of any kind passing through the aurora servers
       *
       * @param {*} _ the root query. an empty object
       * @param {object} input the data from the form
       * @param {object} { cache } the apollo cache from the context object
       * @returns {object}
       */
      makeSecureEmailRequest: async (_, { input }, { cache }) => {
        const data = modifyRequestApptData(input)
        
        // prepare the result object that will be returned from the request.
        const result = {
          __typename: 'SecureEmailResponse',
          response: {},
          appointmentType: input.appointmentType,
          email: input.email,
          lanuguage: input.language,
          phone: input.phone,
          physician: input.physician,
          specialty: input.specialty,
          timeToCallback: input.timeToCallback
        }

        let url = 'https://test-mailformprocessor.uchc.edu/ProcessEmailForm.aspx'

        if (window.location.host === 'health.uconn.edu') {
          url = 'https://mailformprocessor.uchc.edu/ProcessEmailForm.aspx'
        }

        // make a request to post the data to the uchc secure email application
        // await and return the response
        return await postSecureData(url, data).then(({ data }) => {
          return {
            ...result,
            response: data,
          }
        }).catch(err => {
          // return a result where the responses include the error
          console.error(err, 'mail processor error')
          return {
            ...result,
            response: { error: err },
          }
        })
      },
      /**
       * 
       * Stores a local version of the route parameters in the apollo cache
       * 
       * @param {object} - input: the data coming from the route
       * @param {object} - cache: the apollo cache instance
       * 
       * @returns {object} - route params read from the apollo cache
       */
      setRouteParams: (_, { input }, { cache }) => {
        const routeParams = {
          __typename: 'RouteParams',
        }

        // set the route params equal to the values from the input
        for (const property in input) {
          routeParams[property] = input[property] !== undefined ? input[property] : ''
        }

        // write the data to the local cache
        cache.writeData({
          data: {
            routeParams
          }
        })

        // read the data back from the cache to make sure it worked correctly
        return cache.readQuery({ query: require('../graphql/queries/routeParams.gql') })
      }
    }
  }
})

export default apolloClient