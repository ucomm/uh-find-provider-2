# Components

Vue applications are built with re-useable components that manipulate data rather than the DOM. This directory contains all the components necessary to run the client-side application. In general, each component is divided into two sections
- template
- script

## Vue Conventions

### Basics
Vue uses a few symbols to manage data in template files. These include
- `:` this symbol is the shorthand for `v-bind`. In a tag it will look like `<p :class="classSetByJavascript">hello</p>`. This allows vue to change the class of a tag based on data.
- `@` this symbol is shorthand for `v-on`. It responds to event handlers for instance `<button @click="clickHandler">Click</button>`.
- control flow
    - `v-if`
    - `v-else-if`
    - `v-else`
- `v-for` allows for iteration over arrays.

### Templates
Code between the template tags represents the markup for components. It can respond to changes in data and binds data back to the `data` method in the script area. Custom components are generally imported from one file to the next all the way up to the root of the app in `FindProvider.vue`. There are two exceptions.

- [`<ApolloQuery>`](https://apollo.vuejs.org/api/apollo-query.html)
- [`<ApolloMutation>`](https://apollo.vuejs.org/api/apollo-mutation.html)

These components are part of the `vue-apollo` package. If you need to use either of these or the built in `$apollo` or `smart query` properties, it's best to keep them to one per file. That is, avoid mixing a query and mutation in the same component. They can easily pass data between themselves.

### Script
The script tags are where the functional parts of components are managed. This area also allows for imports from other components and exports the current file.