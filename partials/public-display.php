<div id="find-a-provider">
  <find-provider></find-provider>
  <?php
    if (preg_match('/msie|MSIE [0-9]+/i', $_SERVER['HTTP_USER_AGENT'])) {
  ?>
    <p>
      We've detected that you are using Internet Explorer 10 or below. Please update your browser to IE 11, Microsoft Edge, or any other modern browser. 
    </p>
    <p>
      Please select either dental <em>or</em> doctor specialties.
    </p>
  <?php
      include UHFP2R_DIR . '/partials/fallback-display.php';
    }
  ?>
  <noscript>
    <p>
      Please consider updating your browser or turning on javascript to use this application.
    </p>
    <p>
      Please select either dental <em>or</em> doctor specialties.
    </p>
    <?php include UHFP2R_DIR . '/partials/fallback-display.php'; ?>
  </noscript>
</div>