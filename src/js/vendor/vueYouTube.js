import Vue from 'vue'

import VueYouTubeEmbed from 'vue-youtube-embed'

Vue.use(VueYouTubeEmbed, { global: false })

const vueYouTube = new Vue()

export default vueYouTube