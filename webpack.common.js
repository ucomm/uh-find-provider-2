const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const publicPath = process.env.NODE_ENV === 'production' ?
  '/wp-content/plugins/uh-find-provider-2/build/js/' :
  '/content/plugins/uh-find-provider-2/build/js/'

const plugins = [
  new VueLoaderPlugin()
]

if (process.env.NODE_ENV !== 'production') {
  plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    generateStatsFile: true,
    openAnalyzer: false,
    statsFilename: path.resolve(__dirname, 'build', 'js', 'report', 'stats.json'),
    reportFilename: path.resolve(__dirname, 'build', 'js', 'report','report.html')
  }))
}

module.exports = {
  entry: [
    // ensure that core js polyfills are available before the entry script is found
    'core-js/stable', 
    'regenerator-runtime/runtime',
    path.resolve(__dirname, 'src', 'js', 'app.js')
  ],
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    // path - tell webpack where to place files
    path: path.resolve(__dirname, 'build', 'js', 'app.js'),
    // publicPath - allow webpack to find the chunks that are needed by the vue router
    publicPath
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: 'commons',
          chunks: 'initial',
          minChunks: 1
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.vue$/,
        // needed to correctly use the <ApolloQuery /> component
        use: [
          {
            loader: 'vue-loader',
            options: {
              transpileOptions: {
                transforms: {
                  dangerousTaggedTemplateString: true
                }
              }
            }
          }
        ]
      },
      // allow apollo components to get graphql queries from files rather than built in
      // the queries can also be inlined to the components
      {
        test: /\.(graphql|gql)$/,
        use: [
          { 
            loader: 'graphql-tag/loader'
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.graphql', '.gql', '.js', '.json', '.vue']
  },
  plugins
}