<p>
  Adding clinics to the whitelist below will enable their Main Telephone Number to show in the clinic area on an individual profile. If a clinic is not in the whitelist, the general 1-84 number will be displayed.
</p>

<div>
  <label for="add-clinic">Whitelist Clinic Phone Numbers: </label>
  <select multiple size="20" name="" id="add-clinic-select">
    <?php
    foreach ($data['data']['clinics']['nodes'] as $index => $clinic) {
      $clinic_name = $clinic['locationName'] . " (" . $clinic['city'] . ")";
      $html = '<option value="' . $clinic_name . '" id="clinic-' . $index . '" data-phone="' . $clinic['mainTelephoneNumber'] . '">';
      $html .= $clinic_name . " " . $clinic['mainTelephoneNumber'];
      $html .= '</option>';
      echo $html;
    }
    ?>
  </select>
  <div class="button-container">
    <button class="button button-green" id="add-all-clinic-button">Add All Clinics</button>
  </div>
  <div class="button-container">
    <button class="button button-green" id="add-clinic-button">Add Clinic</button>
  </div>
  <div class="button-container">
    <button class="button button-red" id="remove-all-clinic-button">Remove All</button>
  </div>
  <div class="filter-container">
    <label for="whitelist-filter">Filter Clinics</label>
    <input type="text" id="whitelist-filter" class="uhfp-filter" placeholder="Filter Clinics" />
  </div>
  <div>
    <textarea name="uhfp-settings[clinic-whitelist-settings]" id="uhfp-whitelist" cols="30" rows="10" hidden></textarea>
    <ul id="whitelist-clinic-list"></ul>
  </div>
</div>