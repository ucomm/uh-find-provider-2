<p>
  Adding specialties to the blacklist below will prevent them from appearing as search options in the application
</p>
<div>
  <label for="find-provider-specialties">Specialties</label>
  <select name="find-provider-specialties" id="find-provider-specialties" multiple size="20">
    <optgroup label="Dental">
      <?php
      if (count($dental_specs) > 0) {
        foreach ($dental_specs['nodes'] as $key => $spec) {
          include UHFP2R_DIR . '/partials/no-js/option.php';
        }
      }
      ?>
    </optgroup>
    <optgroup label="Doctor">
      <?php
      if (count($doctor_specs) > 0) {
        foreach ($doctor_specs['nodes'] as $key => $spec) {
          include UHFP2R_DIR . '/partials/no-js/option.php';
        }
      }
      ?>
    </optgroup>
  </select>
</div>
<div class="button-container">
  <button class="button button-green" id="add-specialty">Add Specialty</button>
  <button class="button button-red" id="remove-all">Remove All</button>
</div>
<div class="filter-container">
  <label for="specialty-filter">Filter Specialties</label>
  <input type="text" id="specialty-filter" class="uhfp-filter" placeholder="Filter Specialties" />
</div>
<div>
  <textarea hidden name="uhfp-settings[blacklist-settings]" id="uhfp-blacklist" cols="30" rows="10"><?php echo $opts; ?></textarea>
  <ul id="blacklist-item-list"></ul>
</div>