<?php

namespace UHFP\Queries;

class Queries {
  public $url;
  public $query_url = UHFP2R_URL . 'src/js/graphql/queries';
  public function __construct()
  {
    $this->url = HTTP_SCHEME . SERVER_NAME;
    if (SERVER_NAME === 'localhost') {
      $this->url .= '/wordpress';
    }
    $this->url .= '/graphql';
  }

  public function getClinicNamesAndLocations() {
    $query = $this->getQuery($this->query_url . '/location.gql');
    return $this->makeGQLRequest($query);
  }

  public function getCombinedSpecialties()
  {
    $query = $this->getQuery($this->query_url . '/combinedSpecialties.gql');
    return $this->makeGQLRequest($query);
  }

  public function getProvidersBySpecialties(string $category, array $specs) {

    $query = $this->getQuery($this->query_url . '/nojsProvidersByParams.gql');

    $variables = [
      'useParameters' => true,
      'fetchAll' => false,
      'category' => $category,
      'specialties' => $specs,
      'location' => ''
    ];

    return $this->makeGQLRequest($query, $variables);
  }
  /**
   * make a php graphql request with optional variables
   *
   * @param string $query
   * @param array $variables
   * @return WPError|request body
   */
  protected function makeGQLRequest(string $query, array $variables = []) {
    $request_body = [ 'query' => $query ];

    if (count($variables) > 0) {
      $request_body['variables'] = $variables;
    }

    $request = wp_remote_post($this->url, [
      'headers' => [
        'Content-Type' => 'application/json'
      ],
      'body' => wp_json_encode($request_body)
    ]);

    if (is_wp_error($request)) {
      return $request;
    }

    return json_decode($request['body'], true);
  }

  /**
   * Get the graphql query from a file and return it as a string
   * aurora doesn't allow file_get_contents so using curl is more reliable
   *
   * @param string $url
   * @return string
   */
  protected function getQuery(string $url): string {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
  }

  public function displayErrors($errors): bool {
    include UHFP2R_DIR . '/partials/errors.php';
    return false;
  }
}