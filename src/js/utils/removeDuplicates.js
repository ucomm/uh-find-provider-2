export const removeDuplicates = (array, property) => {
  return array.reduce((acc, cur) => {
    if (acc.indexOf(cur[property]) === -1) {
      acc.push(cur[property])
    }
    return acc
  }, [])
}