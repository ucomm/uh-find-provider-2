<?php

namespace UHFP\Admin;

class SettingsPage {

  public function init() {
    add_action('admin_menu', [$this, 'addSubmenuPage']);
  }


  public function addSubmenuPage() {
    add_submenu_page(
      'options-general.php',
      'Find a Provider Settings',
      'Find a Provider Settings',
      'manage_options',
      'uhfp-settings',
      [$this, 'getSettingsPage']
    );
  }

  public function getSettingsPage() {
    include UHFP2R_DIR . '/partials/admin/settings-page.php';
  }
}