<?php

namespace UHFP;

class RewriteHandler
{

  public $slug = '';

  public function __construct(string $slug)
  {
    $this->slug = $slug;
    add_action('init', array($this, 'rewriteRequest'));
    add_action('wp_head', array($this, 'addBaseMeta'));
  }

  public function rewriteRequest()
  {
    add_rewrite_rule('^' . $this->slug . '\/(.*)\/?', 'index.php?pagename=' . $this->slug, 'top');

    flush_rewrite_rules(true);
  }

  public function addBaseMeta()
  {
    if (is_page($this->slug)) {
      echo '<base href="' . parse_url(home_url(), PHP_URL_PATH) . '/' . $this->slug . '/">';
    }
  }
}
